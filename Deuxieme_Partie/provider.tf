terraform {
    required_providers {
      scaleway = {
        source = "scaleway/scaleway"
	       }
    }
    required_version = ">= 0.13" 
  }
provider "scaleway" {
  zone   = "fr-par-1"
  region = "fr-par"
  project_id = "a76a55c8-4679-4050-9dc5-5f1e22ac0fdd"
}

variable "project_id" {
  type        = string
  default = "a76a55c8-4679-4050-9dc5-5f1e22ac0fdd"
  description = "Versailles Hexagone"
}


