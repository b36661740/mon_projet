resource "scaleway_instance_server" "db" {
  name  = "${local.team}-Player1.db"
  type  = "DEV1-S"
  cloud_init = file("${path.module}/init_db.sh")

  image = "debian_buster"
  tags  = ["${local.team}", "db", "mysql"]

  root_volume {
    size_in_gb = 20 
  }

  private_network {
    pn_id = scaleway_vpc_private_network.Player1_vpc.id
  }

  
 
}
resource "scaleway_instance_security_group" "db_security_group" {

  name = "db_security_Player1"
  inbound_default_policy = "drop"
  outbound_default_policy = "accept"

  inbound_rule {
    action = "accept"
    port = 3306
    ip_range = "192.168.42.101/32"
  }

  inbound_rule {
    action = "accept"
    port = 3306
    ip_range =  "192.168.42.102/32"
  }

  

}

// Sortie pour l'adresse IP privée de l'instance de base de données
output "db_private_ip" {
  value = scaleway_instance_server.db.private_ip
}
