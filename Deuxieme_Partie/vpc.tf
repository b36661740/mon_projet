 resource "scaleway_vpc_private_network" "Player1_vpc" {
  // Configuration par défaut pour un réseau VPC privé.
}

resource "scaleway_vpc_public_gateway_ip" "pgw_ip" {
  // Configuration par défaut pour obtenir une adresse IP publique pour la passerelle.
}

resource "scaleway_vpc_public_gateway" "pgw" {
  type = "VPC-GW-S"
  bastion_enabled = true
  ip_id = scaleway_vpc_public_gateway_ip.pgw_ip.id
}

resource "scaleway_vpc_public_gateway_dhcp" "dhcp" {
  subnet = "192.168.42.0/24"
  dns_local_name = "dns-local-name" // Assurez-vous que cette valeur est correcte.
  pool_low = "192.168.42.100"
  pool_high = "192.168.42.200"
}

// Assurez-vous que les noms des instances sont corrects.
// Remplacez `scaleway_instance_server.srv1` et `scaleway_instance_server.srv2` par les vrais noms des instances.
resource "scaleway_vpc_public_gateway_dhcp_reservation" "frontsrv1" {
  gateway_network_id = scaleway_vpc_gateway_network.mygw.id
  mac_address = scaleway_instance_server.back1.private_network[0].mac_address
  ip_address = "192.168.42.101"
}

resource "scaleway_vpc_public_gateway_dhcp_reservation" "frontsrv2" {
  gateway_network_id = scaleway_vpc_gateway_network.mygw.id
  mac_address = scaleway_instance_server.back2.private_network[0].mac_address
  ip_address = "192.168.42.102"
}

resource "scaleway_vpc_gateway_network" "mygw" {
  gateway_id = scaleway_vpc_public_gateway.pgw.id
  private_network_id = scaleway_vpc_private_network.Player1_vpc.id
  dhcp_id = scaleway_vpc_public_gateway_dhcp.dhcp.id
  enable_dhcp = true
}

// Si vous avez une sortie `bastion_ip` définie dans un autre fichier, changez le nom ici ou supprimez la sortie en double.
output "bastion_ip" {
  value = scaleway_vpc_public_gateway_ip.pgw_ip.address
}
