#!/bin/bash

# Mettre à jour les paquets du système.
apt update && apt upgrade -y

# Installer MySQL.
apt install -y mysql-server

# Remplacer les valeurs ci-dessous par vos propres valeurs avant d'exécuter ce script.
MYSQL_ROOT_PASSWORD="azerty"
DRUPAL_DB="db_Player1"
DRUPAL_USER="Player1"
DRUPAL_PASSWORD="azerty123"

# Sécuriser l'installation MySQL (cette méthode est simplifiée).
mysql -e "UPDATE mysql.user SET authentication_string=PASSWORD('$MYSQL_ROOT_PASSWORD') WHERE User='root';"
mysql -e "DELETE FROM mysql.user WHERE User='';"
mysql -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
mysql -e "DROP DATABASE IF EXISTS test;"
mysql -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';"
mysql -e "FLUSH PRIVILEGES;"

# Créer la base de données pour Drupal.
mysql -e "CREATE DATABASE $DRUPAL_DB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"

# Créer l'utilisateur Drupal et lui attribuer les droits nécessaires.
mysql -e "CREATE USER '$DRUPAL_USER'@'localhost' IDENTIFIED BY '$DRUPAL_PASSWORD';"
mysql -e "GRANT ALL PRIVILEGES ON $DRUPAL_DB.* TO '$DRUPAL_USER'@'localhost';"
mysql -e "FLUSH PRIVILEGES;"

# Redémarrer MySQL pour appliquer les changements.
systemctl restart mysql.service

echo "Configuration de la base de données pour Drupal terminée."
