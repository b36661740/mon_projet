// Premier serveur backend pour Drupal
resource "scaleway_instance_server" "back1" {
  name  = "${local.team}-back1"
  type  = "PRO2-XXS"
  image = "debian_bookworm"
  
  user_data ={
	 role       = "web"
	 cloud-init = file("${path.module}/init_srv.sh")
}
  tags  = ["${local.team}", "web", "drupal"]
  root_volume {
    size_in_gb = 20 
  }
  // Inclure le script d'initialisation adapté pour Drupal ici
  enable_ipv6 = false
  private_network {
    pn_id = scaleway_vpc_private_network.Player1_vpc.id
  }
}

// Deuxième serveur backend pour Drupal
resource "scaleway_instance_server" "back2" {
  name  = "${local.team}-back2"
  type  = "PRO2-XXS"
  image = "debian_bookworm"

  user_data ={
	 role       = "web"
	 cloud-init = file("${path.module}/init_srv.sh")}
  tags  = ["${local.team}", "web", "drupal"]
  root_volume {
    size_in_gb = 20
  }
  // Inclure le script d'initialisation adapté pour Drupal ici
  enable_ipv6 = false
  private_network {
    pn_id = scaleway_vpc_private_network.Player1_vpc.id
  }
}

// Sorties pour les adresses IP privées
output "back1_private_ip" {
  value = scaleway_instance_server.back1.private_ip
}

output "back2_private_ip" {
  value = scaleway_instance_server.back2.private_ip
}


