#!/bin/bash

# Mettre à jour la liste des paquets et les paquets existants
apt-get update && apt-get upgrade -y

# Installer Apache, PHP, et les extensions nécessaires pour Drupal
apt-get install -y apache2 php php-mysql libapache2-mod-php php-gd php-xml php-mbstring

# Redémarrer Apache pour s'assurer que toutes les nouvelles configurations sont prises en compte
systemctl restart apache2

# Créer un répertoire pour Drupal et changer les permissions
mkdir -p /var/www/html/drupal
chown -R www-data:www-data /var/www/html/drupal
chmod -R 755 /var/www/html/drupal

# Télécharger la dernière version de Drupal
cd /var/www/html/drupal
wget -O drupal.tar.gz https://www.drupal.org/download-latest/tar.gz
tar -xvzf drupal.tar.gz --strip-components=1
rm drupal.tar.gz

# Modifier le fichier de configuration d'Apache pour servir le site Drupal

cat <<EOF > /etc/apache2/sites-available/drupal.conf
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html/drupal
    <Directory /var/www/html/drupal>
        AllowOverride All
        Order Allow,Deny
        Allow from all
    </Directory>
</VirtualHost>
EOF

# Activer le nouveau site et le module rewrite d'Apache
a2ensite drupal.conf
a2enmod rewrite

# Redémarrer Apache pour activer la nouvelle configuration
systemctl restart apache2

# Afficher un message de fin d'installation
echo "Drupal a été installé avec succès."
