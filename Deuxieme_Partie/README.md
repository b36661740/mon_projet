# Déploiement de Drupal sur Scaleway avec Terraform

## Architecture

Ce projet configure un environnement Drupal sur Scaleway comprenant :
- **VPC** : Isolation des ressources dans un réseau privé.
- **Instances Web** : Deux instances (`PRO2-XXS`) sous Debian pour Drupal.
- **Base de Données** : Une instance (`DEV1-S`) sous Debian pour MySQL.
- **Load Balancer** : Répartition du trafic entre les serveurs web.

## Prérequis

- Compte Scaleway
- Terraform installé
- Connaissance basique de Makefile et Bash

## Mise en Place

### 1. Préparation

Clonez et naviguez dans votre répertoire de travail. Copiez les fichiers de `Projects_Player1` dans ce répertoire.

### 2. Configuration avec Makefile

Utilisez les commandes suivantes pour gérer votre déploiement :

- **Initialiser** : `make init`
- **Planifier** : `make plan`
- **Appliquer** : `make apply`
- **Détruire** : `make destroy`
- **Afficher les Sorties** : `make output`

### 3. Scripts d'Initialisation

- **Base de Données** : `init_db.sh` configure MySQL.
- **Serveurs Web** : `init_srv.sh` prépare les instances avec Apache, PHP, et Drupal
.
## Connexion aux Instances

Pour vous connecter à vos instances via le bastion, utilisez le script `./go` suivi du nom de l'instance :

```bash
./go back1

## Exemple de Connexion à une Instance

Voici un exemple de ce que vous verrez lors de la connexion à l'une des instances web (`back1`) via le script `./go` :

```bash
cloud8@Cloud8:~/Projects_Player1$ ./go back1
IP : 10.76.177.41
Warning: Permanently added '[51.158.99.33]:61000' (ED25519) to the list of known hosts.
Warning: Permanently added '10.76.177.41' (ED25519) to the list of known hosts.
Linux player1-back1 6.1.0-17-cloud-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.69-1 (2023-12-30) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
root@player1-back1:~# ip a

## Accès et Test

Accédez à Drupal via l'IP du Load Balancer. Utilisez `make output` pour récupérer cette adresse.

## Visualisation des Adresses IP avec `make output`

Après avoir appliqué votre configuration avec Terraform, vous pouvez utiliser la commande `make output` pour afficher les adresses IP attribuées à vos ressources. Voici un exemple de sortie que vous pourriez obtenir :

```bash
cloud8@Cloud8:~/Projects_Player1$ make output
tofu output
back1_private_ip = "10.76.177.41"
back2_private_ip = "10.75.19.47"
bastion_ip = "51.158.99.33"
db_private_ip = "10.69.4.1"
lb_ip = "195.154.74.155"


## Notes Importantes

- **Scripts Cloud-init** : Un délai est nécessaire avant d'exécuter les commandes `apt` en raison d'un problème de réseau chez Scaleway.
- **Adresse IP Fixe** : Un redémarrage peut être nécessaire pour la configuration réseau correcte.

## Contribution

Pour contribuer, créez une issue ou soumettez une pull request.

## Licence

Projet sous licence MIT.


