resource "scaleway_instance_ip" "Player1_srv1" {}

resource "scaleway_instance_server" "Player1_srv1" {
    name  = "Player1_srv1"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = ["Player1" ,"web", "drupal" ]
    ip_id = scaleway_instance_ip.Player1_srv1.id
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/init_Player1_srv1.sh")
    }
  }


# Données en sortie

output "srv1_public_ip" {
  value = "${scaleway_instance_server.Player1_srv1.public_ip}"
}

