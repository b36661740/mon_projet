# Déploiement de Drupal avec Terraform sur Scaleway

Ce projet automatise le déploiement d'une instance Scaleway configurée pour exécuter Drupal, un système de gestion de contenu (CMS) populaire, en utilisant Terraform. L'instance est configurée avec Apache, PHP et les extensions nécessaires pour Drupal.

## Prérequis

- Compte Scaleway
- Terraform installé sur votre machine
- Clé API Scaleway configurée avec Terraform

## Structure du Projet

- `instance.tf` : Définition de l'instance Scaleway et de l'adresse IP associée.
- `init_Player1_srv.sh` : Script d'initialisation pour configurer l'instance avec Apache, PHP, et installer Drupal.
- `provider.tf` : Configuration du provider Terraform pour Scaleway.

## Configuration

1. Assurez-vous d'avoir configuré votre clé API Scaleway dans Terraform.
2. Ajustez les paramètres dans `instance.tf` selon vos besoins, tels que le nom de l'instance et le type.

## Déploiement

Pour déployer votre instance Drupal sur Scaleway, suivez ces étapes :

Utilisez les commandes suivantes pour gérer votre déploiement :

- **Initialiser** : `tofu init`
- **Planifier** : `tofu plan`
- **Appliquer** : `tofu apply`
- **Détruire** : `tofu destroy`
- **Afficher les Sorties** : `tofu output`

## Génération et Configuration des Clés SSH

Pour sécuriser l'accès à votre instance Scaleway, il est recommandé de générer une paire de clés SSH. Voici comment procéder :

1. **Vérification de l'existence de clés SSH existantes** : Ouvrez un terminal sur votre système local et exécutez :
ls ~/.ssh/id_rsa*


Si des fichiers sont listés, cela signifie que vous avez déjà une paire de clés SSH et vous pouvez passer à l'étape de copie de la clé publique. Si aucun fichier n'apparaît, suivez l'étape suivante pour générer une nouvelle paire de clés.

2. **Génération d'une nouvelle paire de clés SSH** : Exécutez la commande suivante et appuyez sur entrée à toutes les questions pour les valeurs par défaut :
ssh-keygen -t rsa


Cela génère une nouvelle paire de clés dans le répertoire `~/.ssh` :
-rw------- ... /home/.../.ssh/id_rsa
-rw-r--r-- ... /home/.../.ssh/id_rsa.pub


La clé privée (`id_rsa`) est protégée en lecture, seul son propriétaire peut la lire.

3. **Copie de la clé publique sur Scaleway** : Affichez le contenu de votre clé publique avec :
cat ~/.ssh/id_rsa.pub


Copiez le contenu affiché (commençant par `ssh-rsa AAA...`) dans la console Web Scaleway, sous la section de gestion des clés SSH, avec un nom qui vous identifie clairement.

Une fois votre clé publique ajoutée à votre compte Scaleway, vous pouvez vous connecter à votre instance via SSH en utilisant son adresse IP publique :

ssh root@adresse_ip_de_votre_instance

## Exemple de Connexion SSH à l'Instance

Une fois que vous avez configuré votre clé SSH et lancé votre instance, vous pouvez vous y connecter via SSH. Voici un exemple de session de connexion réussie :

```bash
cloud8@Cloud8:~/Project_Player1$ ssh root@51.158.79.252
Linux player1-srv1 6.1.0-17-cloud-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.69-1 (2023-12-30) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Sun Feb 11 19:04:57 2024 from 46.193.69.6
root@player1-srv1:~# whoami
root
root@player1-srv1:~# who
root     pts/0        2024-02-11 19:28 (46.193.69.6)
root@player1-srv1:~#


