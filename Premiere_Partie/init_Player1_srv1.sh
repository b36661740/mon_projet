#!/bin/bash

# Mise à jour des paquets
apt-get update && apt-get upgrade -y

# Installation d'Apache2 et PHP avec les extensions nécessaires pour Drupal
apt-get install -y apache2 php php-cli php-fpm php-json php-common php-mysql php-zip php-gd php-mbstring php-curl php-xml php-pear php-bcmath

# Réglages recommandés pour php.ini, à ajuster selon vos besoins
sed -i "s/upload_max_filesize = .*/upload_max_filesize = 50M/" /etc/php/*/apache2/php.ini
sed -i "s/post_max_size = .*/post_max_size = 50M/" /etc/php/*/apache2/php.ini
sed -i "s/memory_limit = .*/memory_limit = 256M/" /etc/php/*/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php/*/apache2/php.ini
sed -i "s/max_execution_time = .*/max_execution_time = 300/" /etc/php/*/apache2/php.ini

# Activation de la réécriture d'URL
a2enmod rewrite

# Redémarrage d'Apache pour prendre en compte les modifications
systemctl restart apache2

# Téléchargement et installation de la dernière version stable de Drupal
cd /var/www/html
wget https://www.drupal.org/download-latest/tar.gz -O drupal.tar.gz
tar -zxvf drupal.tar.gz --strip-components=1
rm drupal.tar.gz

# Modification des permissions pour que Drupal puisse gérer les fichiers
chown -R www-data:www-data /var/www/html
find /var/www/html -type d -exec chmod 755 {} \;
find /var/www/html -type f -exec chmod 644 {} \;

# Suppression de la page index.html par défaut d'Apache
rm index.html

# Création d'un fichier .htaccess si Drupal ne l'a pas déjà fait
if [ ! -f .htaccess ]; then
    cat > .htaccess <<EOF
# Suivez les instructions de la documentation de Drupal pour le fichier .htaccess
# https://www.drupal.org/docs/7/configuring-clean-urls/enable-clean-urls
EOF
fi

# Nettoyage
apt-get clean

echo "L'installation de Drupal sur l'instance est terminée."
